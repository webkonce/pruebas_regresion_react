## Ruta

cd ./frontend

## install dependencies

After that project was downloaded, execute the following command on you terminal `npm install` to install your dependencies.

## run server

Run `npm start` for a dev server. Navigate to `http://localhost:3001/`. The app will automatically reload if you change any of the source files.
