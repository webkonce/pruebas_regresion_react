import axios from "axios";
class EasyServiceClass {
  //url: string;
  private test: string;
  constructor(test: string) {
    this.test = test;
  }
  //APRO
  async EasyAPRO() {
    /*** Easy SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "charmander_1990@gmail.com",
          cuotas: "1",
        },
      });
      console.log(data);
      return data;
      //return data;
    } catch (error) {
      console.log(error);
    }
  }

  //CONT
  async EasyCONT() {
    /*** Easy SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "charmander_1990@gmail.com",
          cuotas: "1",
        },
      });
      console.log(data);
      return data;
    } catch (error) {
      console.log(error);
    }
  }
  //OTHE
  async EasyOTHE() {
    /*** Easy SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "charmander_1990@gmail.com",
          cuotas: "1",
        },
      });
      console.log(data);
      return data;
    } catch (error) {
      console.log(error);
    }
  }

  //CALL
  async EasyCALL() {
    /*** Easy SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "charmander_1990@gmail.com",
          cuotas: "1",
        },
      });
      console.log(data);
      return data;
    } catch (error) {
      console.log(error);
    }
  }

  //FUND
  async EasyFUND() {
    /*** Easy SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "charmander_1990@gmail.com",
          cuotas: "1",
        },
      });
      console.log(data);
      return data;
    } catch (error) {
      console.log(error);
    }
  }

  //SECU
  async EasySECU() {
    /*** Easy SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "charmander_1990@gmail.com",
          cuotas: "1",
        },
      });
      console.log(data);
      return data;
    } catch (error) {
      console.log(error);
    }
  }

  //EXPI
  async EasyEXPI() {
    /*** Easy SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "charmander_1990@gmail.com",
          cuotas: "1",
        },
      });
      console.log(data);
      return data;
    } catch (error) {
      console.log(error);
    }
  }

  //FORM
  async EasyFORM() {
    /*** Easy SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "charmander_1990@gmail.com",
          cuotas: "1",
        },
      });
      console.log(data);
      return data;
    } catch (error) {
      console.log(error);
    }
  }

  async agregarProductoAlCarrito() {
    /*** Easy CARITO SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "charmander_1990@gmail.com",
          cuotas: "1",
        },
      });
      console.log(data);
      return data;
    } catch (error) {
      console.log(error);
    }
  }
}

export class EasyService extends EasyServiceClass {
  constructor(test: string) {
    super(test);
  }
  agregarSKUCarrito() {
    try {
      return this.agregarProductoAlCarrito();
    } catch (error) {
      console.log(error);
    }
  }
  //OPEN PAGE
  tarjetaVisa_EasyAPRO() {
    try {
      return this.EasyAPRO();
    } catch (error) {
      console.log(error);
    }
  }

  tarjetaVisa_EasyCONT() {
    try {
      return this.EasyCONT();
    } catch (error) {
      console.log(error);
    }
  }
  tarjetaVisa_EasyOTHE() {
    try {
      return this.EasyOTHE();
    } catch (error) {
      console.log(error);
    }
  }
  tarjetaVisa_EasyCALL() {
    try {
      return this.EasyCALL();
    } catch (error) {
      console.log(error);
    }
  }
  tarjetaVisa_EasyFUND() {
    try {
      return this.EasyFUND();
    } catch (error) {
      console.log(error);
    }
  }
  tarjetaVisa_EasySECU() {
    try {
      return this.EasySECU();
    } catch (error) {
      console.log(error);
    }
  }
  tarjetaVisa_EasyEXPI() {
    try {
      return this.EasyEXPI();
    } catch (error) {
      console.log(error);
    }
  }
  tarjetaVisa_EasyFORM() {
    try {
      return this.EasyFORM();
    } catch (error) {
      console.log(error);
    }
  }

  tarjetaMastercard_EasyAPRO() {
    try {
      return this.EasyAPRO();
    } catch (error) {
      console.log(error);
    }
  }

  tarjetaMastercard_EasyCONT() {
    try {
      return this.EasyCONT();
    } catch (error) {
      console.log(error);
    }
  }
  tarjetaMastercard_EasyOTHE() {
    try {
      return this.EasyOTHE();
    } catch (error) {
      console.log(error);
    }
  }
  tarjetaMastercard_EasyCALL() {
    try {
      return this.EasyCALL();
    } catch (error) {
      console.log(error);
    }
  }
  tarjetaMastercard_EasyFUND() {
    try {
      return this.EasyFUND();
    } catch (error) {
      console.log(error);
    }
  }
  tarjetaMastercard_EasySECU() {
    try {
      return this.EasySECU();
    } catch (error) {
      console.log(error);
    }
  }
  tarjetaMastercard_EasyEXPI() {
    try {
      return this.EasyEXPI();
    } catch (error) {
      console.log(error);
    }
  }
  tarjetaMastercard_EasyFORM() {
    try {
      return this.EasyFORM();
    } catch (error) {
      console.log(error);
    }
  }
}
