import axios from "axios";
class SisaServiceClass {
  //url: string;
  private test: string;
  constructor(test: string) {
    this.test = test;
  }
  //UPDATE FAVORITE CARD
  async catOneClickAprobadoSisaService() {
    /*** Sisa SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "francisco.roavalenzuela@externos-cl.cencosud.com",
          ruta: "C:/Users/Francisco/Desktop/cencosud/katalon/Reportes/",
        },
      });
      console.log(data);
      return data;
      //peticionGet();
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      //console.log("*** CuartoPaso *** MANEJAR EL PEDIDO EN VTEX ");
      //console.log(data);
      //return data;
    } catch (error) {
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      // ERROR RESPONSE VTEX SERVICE OP FORM Order invalid
      //console.log("*** Tercer Paso *** Get test ");
      //console.log(error.response.request);
      //console.log(error);
    }
  }

  //UPDATE FAVORITE CARD
  async SisaOneClickAprobadoSisaService() {
    /*** Sisa SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "francisco.roavalenzuela@externos-cl.cencosud.com",
          ruta: "C:/Users/Francisco/Desktop/cencosud/katalon/Reportes/",
        },
      });
      console.log(data);
      return data;
      //peticionGet();
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      //console.log("*** CuartoPaso *** MANEJAR EL PEDIDO EN VTEX ");
      //console.log(data);
      //return data;
    } catch (error) {
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      // ERROR RESPONSE VTEX SERVICE OP FORM Order invalid
      //console.log("*** Tercer Paso *** Get test ");
      //console.log(error.response.request);
      //console.log(error);
    }
  }
  //UPDATE FAVORITE CARD
  async debitoOneClickAprobadoSisaService() {
    /*** Sisa SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "francisco.roavalenzuela@externos-cl.cencosud.com",
          ruta: "C:/Users/Francisco/Desktop/cencosud/katalon/Reportes/",
        },
      });
      console.log(data);
      return data;
      //peticionGet();
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      //console.log("*** CuartoPaso *** MANEJAR EL PEDIDO EN VTEX ");
      //console.log(data);
      //return data;
    } catch (error) {
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      // ERROR RESPONSE VTEX SERVICE OP FORM Order invalid
      //console.log("*** Tercer Paso *** Get test ");
      //console.log(error.response.request);
      //console.log(error);
    }
  }
  async jumboOneClickAprobadoSisaService() {
    /*** Sisa SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "francisco.roavalenzuela@externos-cl.cencosud.com",
          ruta: "C:/Users/Francisco/Desktop/cencosud/katalon/Reportes/",
        },
      });
      console.log(data);
      return data;
      //peticionGet();
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      //console.log("*** CuartoPaso *** MANEJAR EL PEDIDO EN VTEX ");
      //console.log(data);
      //return data;
    } catch (error) {
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      // ERROR RESPONSE VTEX SERVICE OP FORM Order invalid
      //console.log("*** Tercer Paso *** Get test ");
      //console.log(error.response.request);
      //console.log(error);
    }
  }
}

export class SisaService extends SisaServiceClass {
  constructor(test: string) {
    super(test);
  }
  //OPEN PAGE
  catOneClick_SisaBo() {
    try {
      return this.catOneClickAprobadoSisaService();
    } catch (error) {
      console.log(error);
    }
  }

  SisaOneClick_SisaBo() {
    try {
      return this.SisaOneClickAprobadoSisaService();
    } catch (error) {
      console.log(error);
    }
  }
  debitoOneClick_SisaBo() {
    try {
      return this.debitoOneClickAprobadoSisaService();
    } catch (error) {
      console.log(error);
    }
  }
  jumboOneclick_SisaBo() {
    try {
      return this.jumboOneClickAprobadoSisaService();
    } catch (error) {
      console.log(error);
    }
  }
}
