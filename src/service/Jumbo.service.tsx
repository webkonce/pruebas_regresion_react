import axios from "axios";
class JumboServiceClass {
  //url: string;
  private test: string;
  constructor(test: string) {
    this.test = test;
  }
  //UPDATE FAVORITE CARD
  async catOneClickAprobadoJumboService() {
    /*** JUMBO SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "francisco.roavalenzuela@externos-cl.cencosud.com",
          ruta: "C:/Users/Francisco/Desktop/cencosud/katalon/Reportes/",
        },
      });
      console.log(data);
      return data;
      //peticionGet();
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      //console.log("*** CuartoPaso *** MANEJAR EL PEDIDO EN VTEX ");
      //console.log(data);
      //return data;
    } catch (error) {
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      // ERROR RESPONSE VTEX SERVICE OP FORM Order invalid
      //console.log("*** Tercer Paso *** Get test ");
      //console.log(error.response.request);
      //console.log(error);
    }
  }

  //UPDATE FAVORITE CARD
  async jumboOneClickAprobadoJumboService() {
    /*** JUMBO SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "francisco.roavalenzuela@externos-cl.cencosud.com",
          ruta: "C:/Users/Francisco/Desktop/cencosud/katalon/Reportes/",
        },
      });
      console.log(data);
      return data;
      //peticionGet();
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      //console.log("*** CuartoPaso *** MANEJAR EL PEDIDO EN VTEX ");
      //console.log(data);
      //return data;
    } catch (error) {
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      // ERROR RESPONSE VTEX SERVICE OP FORM Order invalid
      //console.log("*** Tercer Paso *** Get test ");
      //console.log(error.response.request);
      //console.log(error);
    }
  }
  //UPDATE FAVORITE CARD
  async debitoOneClickAprobadoJumboService() {
    /*** JUMBO SERVICE CONFIGURATION  ****/
    try {
      const { data } = await axios({
        method: "put",
        url: `http://localhost:3000/${this.test}`,
        data: {
          username: "francisco.roavalenzuela@externos-cl.cencosud.com",
          ruta: "C:/Users/Francisco/Desktop/cencosud/katalon/Reportes/",
        },
      });
      console.log(data);
      return data;
      //peticionGet();
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      //console.log("*** CuartoPaso *** MANEJAR EL PEDIDO EN VTEX ");
      //console.log(data);
      //return data;
    } catch (error) {
      //console.log({ vtex_test_harcodeada: `1115981197277-01` });
      // ERROR RESPONSE VTEX SERVICE OP FORM Order invalid
      //console.log("*** Tercer Paso *** Get test ");
      //console.log(error.response.request);
      //console.log(error);
    }
  }
}

export class JumboService extends JumboServiceClass {
  constructor(test: string) {
    super(test);
  }
  //OPEN PAGE
  catOneClick_JumboBo() {
    try {
      return this.catOneClickAprobadoJumboService();
    } catch (error) {
      console.log(error);
    }
  }

  jumboOneClick_JumboBo() {
    try {
      return this.jumboOneClickAprobadoJumboService();
    } catch (error) {
      console.log(error);
    }
  }
  debitoOneClick_JumboBo() {
    try {
      return this.debitoOneClickAprobadoJumboService();
    } catch (error) {
      console.log(error);
    }
  }
}
