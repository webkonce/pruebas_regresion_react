import React, { Fragment, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { Card } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Table from "@material-ui/core/Table";
import Modal from "@material-ui/core/Modal";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Input from "@material-ui/core/Input";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import AccountCircle from "@material-ui/icons/AccountCircle";
import axios from "axios";
import "./Inicio.css";
import { StringifyOptions } from "querystring";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import { JumboService } from "../../service/Jumbo.service";
import { SisaService } from "../../service/Sisa.service";
import { EasyService } from "../../service/Easy.service";
//const baseUrl = `${process.env.REACT_APP_HEROE_URL}/superheroes`;
//const [data, setData] = useState([{}]);
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  table: {
    minWidth: 650,
  },
  margin: {
    margin: theme.spacing(1),
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

function createData(
  name: string,
  calories: number,
  fat: number,
  carbs: number,
  protein: number
) {
  return { name, calories, fat, carbs, protein };
}

function handleSubmit() {}

function viewHeroeDetails(id: number) {
  console.log(id);
}

/*useEffect(() => {
  //peticionGet();
}, []);*/

export default function Inicio(): JSX.Element {
  const [data, setData] = useState([]);

  async function catOneclickJumboBOAprovado() {
    const JUMBOService = new JumboService("jumbo/catoneclick");
    const respuesta = await JUMBOService.catOneClick_JumboBo();
    console.log(respuesta);
    setData(respuesta);
  }

  async function JumboOneclickJumboBOAprovado() {
    const JUMBOService = new JumboService("jumbo/jumbooneclick");
    const respuesta = await JUMBOService.jumboOneClick_JumboBo();
    console.log(respuesta);
    setData(respuesta);
  }

  async function DebitoOneclickJumboBOAprovado() {
    const JUMBOService = new JumboService("jumbo/debitooneclick");
    const respuesta = await JUMBOService.debitoOneClick_JumboBo();
    console.log(respuesta);
    setData(respuesta);
  }

  async function jumboOneclickSisaBOAprovado() {
    const SISAService = new SisaService("sisa/jumbooneclick");
    const respuesta = await SISAService.jumboOneclick_SisaBo();
    console.log(respuesta);
    setData(respuesta);
  }

  async function catOneclickSisaBOAprovado() {
    const SISAService = new SisaService("sisa/catoneclick");
    const respuesta = await SISAService.catOneClick_SisaBo();
    console.log(respuesta);
    setData(respuesta);
  }

  async function debitoOneclickSisaBOAprovado() {
    const SISAService = new SisaService("sisa/debitooneclick");
    const respuesta = await SISAService.debitoOneClick_SisaBo();
    console.log(respuesta);
    setData(respuesta);
  }
  /** CARRITO */
  async function agregarSKUalCarritoCompra() {
    const EASYService = new EasyService("easy/carrito/agregar_sku");
    const respuesta = await EASYService.agregarSKUCarrito();
    console.log(respuesta);
    setData(respuesta);
  }

  /** VISA */

  async function easyTarjetaVisaAPRO() {
    const EASYService = new EasyService("easy/visa/apro");
    const respuesta = await EASYService.tarjetaVisa_EasyAPRO();
    console.log(respuesta);
    setData(respuesta);
  }

  async function easyTarjetaVisaCONT() {
    const EASYService = new EasyService("easy/visa/cont");
    const respuesta = await EASYService.tarjetaVisa_EasyAPRO();
    console.log(respuesta);
    setData(respuesta);
  }

  async function easyTarjetaVisaOTHE() {
    const EASYService = new EasyService("easy/visa/othe");
    const respuesta = await EASYService.tarjetaVisa_EasyOTHE();
    console.log(respuesta);
    setData(respuesta);
  }

  async function easyTarjetaVisaCALL() {
    const EASYService = new EasyService("easy/visa/call");
    const respuesta = await EASYService.tarjetaVisa_EasyCALL();
    console.log(respuesta);
    setData(respuesta);
  }

  async function easyTarjetaVisaFUND() {
    const EASYService = new EasyService("easy/visa/fund");
    const respuesta = await EASYService.tarjetaVisa_EasyFUND();
    console.log(respuesta);
    setData(respuesta);
  }

  async function easyTarjetaVisaSECU() {
    const EASYService = new EasyService("easy/visa/secu");
    const respuesta = await EASYService.tarjetaVisa_EasySECU();
    console.log(respuesta);
    setData(respuesta);
  }

  async function easyTarjetaVisaEXPI() {
    const EASYService = new EasyService("easy/visa/expi");
    const respuesta = await EASYService.tarjetaVisa_EasyEXPI();
    console.log(respuesta);
    setData(respuesta);
  }
  async function easyTarjetaVisaFORM() {
    const EASYService = new EasyService("easy/visa/form");
    const respuesta = await EASYService.tarjetaVisa_EasyFORM();
    console.log(respuesta);
    setData(respuesta);
  }

  // MASTERCARD

  async function easyTarjetaMastercardAPRO() {
    const EASYService = new EasyService("easy/mastercard/apro");
    const respuesta = await EASYService.tarjetaMastercard_EasyAPRO();
    console.log(respuesta);
    setData(respuesta);
  }

  async function easyTarjetaMastercardCONT() {
    const EASYService = new EasyService("easy/mastercard/cont");
    const respuesta = await EASYService.tarjetaMastercard_EasyAPRO();
    console.log(respuesta);
    setData(respuesta);
  }

  async function easyTarjetaMastercardOTHE() {
    const EASYService = new EasyService("easy/mastercard/othe");
    const respuesta = await EASYService.tarjetaMastercard_EasyOTHE();
    console.log(respuesta);
    setData(respuesta);
  }

  async function easyTarjetaMastercardCALL() {
    const EASYService = new EasyService("easy/mastercard/call");
    const respuesta = await EASYService.tarjetaMastercard_EasyCALL();
    console.log(respuesta);
    setData(respuesta);
  }

  async function easyTarjetaMastercardFUND() {
    const EASYService = new EasyService("easy/mastercard/fund");
    const respuesta = await EASYService.tarjetaMastercard_EasyFUND();
    console.log(respuesta);
    setData(respuesta);
  }

  async function easyTarjetaMastercardSECU() {
    const EASYService = new EasyService("easy/mastercard/secu");
    const respuesta = await EASYService.tarjetaMastercard_EasySECU();
    console.log(respuesta);
    setData(respuesta);
  }

  async function easyTarjetaMastercardEXPI() {
    const EASYService = new EasyService("easy/mastercard/expi");
    const respuesta = await EASYService.tarjetaMastercard_EasyEXPI();
    console.log(respuesta);
    setData(respuesta);
  }
  async function easyTarjetaMastercardFORM() {
    const EASYService = new EasyService("easy/mastercard/form");
    const respuesta = await EASYService.tarjetaMastercard_EasyFORM();
    console.log(respuesta);
    setData(respuesta);
  }

  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  /**
   *               <div>
                <button type="button" onClick={handleOpen}>
                  Open Modal
                </button>
                <Modal
                  open={open}
                  onClose={handleClose}
                  aria-labelledby="simple-modal-title"
                  aria-describedby="simple-modal-description"
                >
                  {body}
                </Modal>
              </div>
   */

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title">Text in a modal</h2>
      <p id="simple-modal-description">
        Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
      </p>
    </div>
  );

  return (
    <Fragment>
      <div className="row">
        <div className="col">
          <div className="card animated fadeIn">
            <div className="card-body">
              <div className={classes.root}>
                <ul className="list-group list-group-flush">
                  <li className="list-group-item list-group-item-success text-center">
                    <strong>JUMBO</strong>
                  </li>
                </ul>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="JUMBO1a-header"
                  >
                    <Typography className={classes.heading}>
                      <span className="ml-2 badge badge-primary badge-pill badge-secondary">
                        <em>
                          <abbr
                            title="Pruebas de pago con Jumbo"
                            className="initialism"
                          >
                            JUMBO-BO-APROBADO
                          </abbr>

                          <i className="fa fa-eye text-white pl-1"></i>
                        </em>
                      </span>
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item list-group-item-success text-center">
                        <strong>PRUEBAS DE PAGO JUMBO</strong>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-block btn-pill btn-sm"
                          onClick={catOneclickJumboBOAprovado}
                        >
                          <em>
                            JUMBO BO APROBADO
                            <i className="fa fa-eye text-white pl-1"></i>
                          </em>
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            CATONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-success">
                            APROBADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={JumboOneclickJumboBOAprovado}
                        >
                          JUMBO-BO-APROBADO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            JUMBOONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-success">
                            APROBADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={DebitoOneclickJumboBOAprovado}
                        >
                          JUMBO-BO-APROBADO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            DEBITOONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-success">
                            APROBADO
                          </span>
                        </a>
                      </li>
                    </ul>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2a-content"
                    id="JUMBO2a-header"
                  >
                    <Typography className={classes.heading}>
                      <span className="ml-2 badge badge-primary badge-pill badge-secondary">
                        <em>
                          <abbr
                            title="Pruebas de pago con Jumbo-Vtex"
                            className="initialism"
                          >
                            JUMBO-VTEX
                          </abbr>

                          <i className="fa fa-eye text-white pl-1"></i>
                        </em>
                      </span>
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item list-group-item-success text-center">
                        <strong>PRUEBAS DE PAGO JUMBO-VTEX</strong>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-block btn-pill btn-sm">
                          <em>
                            JUMBO-VTEX-CANCELADO
                            <i className="fa fa-eye text-white pl-1"></i>
                          </em>
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            CATONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-danger">
                            CANCELADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-block btn-pill btn-sm">
                          JUMBO-VTEX-FACTURADO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            CATONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-dark text-white">
                            FACTURADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-block btn-pill btn-sm">
                          JUMBO-VTEX-REEMBOLSO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            CATONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-primary">
                            REEMBOLSO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-block btn-pill btn-sm">
                          JUMBO-VTEX-CANCELADO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            JUMBOONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-danger">
                            CANCELADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-block btn-pill btn-sm">
                          JUMBO-VTEX-REEMBOLSO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            JUMBOONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-primary">
                            REEMBOLSO
                          </span>
                        </a>
                      </li>
                    </ul>
                  </AccordionDetails>
                </Accordion>
                <Accordion disabled>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel3a-content"
                    id="JUMBO3a-header"
                  >
                    <Typography className={classes.heading}></Typography>
                  </AccordionSummary>
                </Accordion>
              </div>
            </div>
          </div>
          <div className="card my-2 animated fadeIn">
            <div className="card-body">
              <div className={classes.root}>
                <ul className="list-group list-group-flush">
                  <li className="list-group-item list-group-item-danger text-center">
                    <strong>SANTA ISABEL (SISA)</strong>
                  </li>
                </ul>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="SISA1a-header"
                  >
                    <Typography className={classes.heading}>
                      <span className="ml-2 badge badge-primary badge-pill badge-secondary">
                        <em>
                          <abbr
                            title="Pruebas de pago con SISA"
                            className="initialism"
                          >
                            SISA-BO-APROBADO
                          </abbr>

                          <i className="fa fa-eye text-white pl-1"></i>
                        </em>
                      </span>
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item list-group-item-danger text-center">
                        <strong>PRUEBAS DE PAGO SANTA ISABEL (SISA)</strong>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={catOneclickSisaBOAprovado}
                        >
                          <em>
                            SISA-BO-APROBADO
                            <i className="fa fa-eye text-white pl-1"></i>
                          </em>
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            CATONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-success">
                            APROBADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={jumboOneclickSisaBOAprovado}
                        >
                          SISA-BO-APROBADO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            JUMBOONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-success">
                            APROBADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={debitoOneclickSisaBOAprovado}
                        >
                          SISA-BO-APROBADO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            DEBITOONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-success">
                            APROBADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-block btn-pill btn-sm">
                          SISA-BO-APROBADO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            SISACATONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-success">
                            APROBADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-block btn-pill btn-sm">
                          SISA-BO-APROBADO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            SISAONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-success">
                            APROBADO
                          </span>
                        </a>
                      </li>
                    </ul>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2a-content"
                    id="SISA2a-header"
                  >
                    <Typography className={classes.heading}>
                      <span className="ml-2 badge badge-primary badge-pill badge-secondary">
                        <em>
                          <abbr
                            title="Pruebas de pago con SISA-Vtex"
                            className="initialism"
                          >
                            SISA-VTEX
                          </abbr>

                          <i className="fa fa-eye text-white pl-1"></i>
                        </em>
                      </span>
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item list-group-item-danger text-center">
                        <strong>PRUEBAS DE PAGO SISA-VTEX</strong>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-pill btn-sm">
                          SISA-VTEX-FACTURADO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            SISACATONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-dark">
                            FACTURADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-pill btn-sm">
                          <em>
                            SISA-VTEX-FACTURADO-REEMBOLSO
                            <i className="fa fa-eye text-white pl-1"></i>
                          </em>
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            SISACATONECLICK
                          </span>
                          <span className="ml-2 mr-2 badge badge-primary badge-pill badge-dark">
                            FACTURADO
                          </span>
                          -
                          <span className="ml-2 badge badge-primary badge-pill badge-primary">
                            REEMBOLSO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-pill btn-sm">
                          SISA-VTEX-CANCELADO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            SISACATONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-danger">
                            CANCELADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-pill btn-sm">
                          SISA-VTEX-CANCELADO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            SISAONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-danger">
                            CANCELADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-pill btn-sm">
                          SISA-VTEX-FACTURADO
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            SISAONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-dark">
                            FACTURADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-pill btn-sm">
                          <em>
                            SISA-VTEX-CANCELADO-LACISTERNA
                            <i className="fa fa-eye text-white pl-1"></i>
                          </em>
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            SISAONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-primary">
                            CANCELADO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a className="btn btn-secondary btn-pill btn-sm">
                          <em>
                            SISA-VTEX-FACTURADO-LACISTERNA
                            <i className="fa fa-eye text-white pl-1"></i>
                          </em>
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            SISAONECLICK
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-dark">
                            FACTURADO
                          </span>
                        </a>
                      </li>
                    </ul>
                  </AccordionDetails>
                </Accordion>
                <Accordion disabled>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel3a-content"
                    id="SISA3a-header"
                  >
                    <Typography className={classes.heading}></Typography>
                  </AccordionSummary>
                </Accordion>
              </div>
            </div>
          </div>
        </div>
        <div className="col">
          <Card>
            <div className="card row animated fadeIn wide mx-1">
              <div className="card-header bg-dark text-white text-center">
                <h1> Respuesta </h1>
              </div>
              <div className="card-body text-center">
                <blockquote className="blockquote">
                  <p className="mb-0">
                    <abbr title="Pruebas Jumbo y Sisa" className="initialism">
                      Pruebas de regresión
                    </abbr>
                  </p>
                </blockquote>
              </div>
            </div>
          </Card>
        </div>
        <div className="col">
          <div className="card animated fadeIn">
            <div className="card-body">
              <div className={classes.root}>
                <ul className="list-group list-group-flush">
                  <li className="list-group-item list-group-item-warning text-center text-danger">
                    <strong>EASY</strong>
                  </li>
                </ul>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="EASY1a-header"
                  >
                    <Typography className={classes.heading}>
                      <span className="ml-2 badge badge-primary badge-pill badge-secondary">
                        <em>
                          <abbr
                            title="TARJETA DE CREDITO VISA"
                            className="initialism"
                          >
                            AGREGAR PRODUCTO AL CARRITO DE COMPRA
                          </abbr>
                          <i className="fa fa-eye text-white pl-1"></i>
                        </em>
                      </span>
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item list-group-item-primary text-center">
                        <strong>CARRITO DE COMPRA EASY</strong>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={agregarSKUalCarritoCompra}
                        >
                          <em>
                            AGREGAR PRODUCTO
                            <i className="fa fa-eye text-white pl-1"></i>
                          </em>
                          <span className="ml-2 badge badge-primary badge-pill badge-info text-dark">
                            clavos Economicos
                          </span>
                        </a>
                      </li>
                    </ul>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="EASY1a-header"
                  >
                    <Typography className={classes.heading}>
                      <span className="ml-2 badge badge-primary badge-pill badge-secondary">
                        <em>
                          <abbr
                            title="TARJETA DE CREDITO VISA"
                            className="initialism"
                          >
                            EASY-TARJETA-VISA
                          </abbr>
                          <i className="fa fa-eye text-white pl-1"></i>
                        </em>
                      </span>
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item list-group-item-primary text-center">
                        <strong>PRUEBAS DE PAGO EASY CON TARJETA VISA</strong>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaVisaAPRO}
                        >
                          <em>
                            EASY-APRO
                            <i className="fa fa-eye text-white pl-1"></i>
                          </em>
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-VISA
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-success">
                            APRO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaVisaCONT}
                        >
                          EASY-CONT
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-VISA
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-dark text-white">
                            CONT
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaVisaOTHE}
                        >
                          EASY-OTHE
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-VISA
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-light text-primary">
                            OTHE
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaVisaCALL}
                        >
                          EASY-CALL
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-VISA
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-secondary text-dark">
                            CALL
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaVisaFUND}
                        >
                          EASY-FUND
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-VISA
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-primary">
                            FUND
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaVisaSECU}
                        >
                          EASY-SECU
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-VISA
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-light text-danger">
                            SECU
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaVisaEXPI}
                        >
                          EASY-EXPI
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-VISA
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-dark text-white">
                            EXPI
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaVisaFORM}
                        >
                          EASY-FORM
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-VISA
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-light text-dark">
                            FORM
                          </span>
                        </a>
                      </li>
                    </ul>
                  </AccordionDetails>
                </Accordion>
                <Accordion>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2a-content"
                    id="EASY2a-header"
                  >
                    <Typography className={classes.heading}>
                      <span className="ml-2 badge badge-primary badge-pill badge-secondary">
                        <em>
                          <abbr
                            title="TARJETA DE CREDITO MASTERCARD"
                            className="initialism"
                          >
                            EASY-TARJETA-MASTERCARD
                          </abbr>
                          <i className="fa fa-eye text-white pl-1"></i>
                        </em>
                      </span>
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item list-group-item-primary text-center">
                        <strong>
                          PRUEBAS DE PAGO EASY CON TARJETA MASTERCARD
                        </strong>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaMastercardAPRO}
                        >
                          <em>
                            EASY-APRO
                            <i className="fa fa-eye text-white pl-1"></i>
                          </em>
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-MASTERCARD
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-success">
                            APRO
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaMastercardCONT}
                        >
                          EASY-CONT
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-MASTERCARD
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-dark text-white">
                            CONT
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaMastercardOTHE}
                        >
                          EASY-OTHE
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-MASTERCARD
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-light text-primary">
                            OTHE
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaMastercardCALL}
                        >
                          EASY-CALL
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-MASTERCARD
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-secondary text-dark">
                            CALL
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaMastercardFUND}
                        >
                          EASY-FUND
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-MASTERCARD
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-primary">
                            FUND
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaMastercardSECU}
                        >
                          EASY-SECU
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-MASTERCARD
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-light text-danger">
                            SECU
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaMastercardEXPI}
                        >
                          EASY-EXPI
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-MASTERCARD
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-dark text-white">
                            EXPI
                          </span>
                        </a>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center">
                        <a
                          className="btn btn-secondary btn-block btn-pill btn-sm"
                          onClick={easyTarjetaMastercardFORM}
                        >
                          EASY-FORM
                          <span className="ml-2 badge badge-primary badge-pill badge-warning">
                            TARJETA-MASTERCARD
                          </span>
                          <span className="ml-2 badge badge-primary badge-pill badge-light text-dark">
                            FORM
                          </span>
                        </a>
                      </li>
                    </ul>
                  </AccordionDetails>
                </Accordion>
                <Accordion disabled>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel3a-content"
                    id="EASY3a-header"
                  >
                    <Typography className={classes.heading}></Typography>
                  </AccordionSummary>
                </Accordion>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-header bg-dark text-white text-center">
              <h1>Campos Requeridos</h1>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="ml-2">
                  <div className="col-md-12">
                    <FormControl
                      fullWidth
                      className={classes.margin}
                      variant="outlined"
                    >
                      <InputLabel htmlFor="email">Email</InputLabel>
                      <OutlinedInput
                        id="email"
                        value="francisco.roavalenzuela@externos-cl.cencosud.com"
                        readOnly
                        startAdornment={
                          <InputAdornment position="start">
                            <AccountCircle />
                          </InputAdornment>
                        }
                        labelWidth={60}
                      />
                    </FormControl>
                    <FormControl fullWidth className={classes.margin}>
                      <InputLabel htmlFor="Ruta_Reporte"></InputLabel>

                      <TextField
                        id="Ruta_Reporte"
                        label="Ruta Reporte"
                        value="C:/Users/Francisco/Desktop/cencosud/katalon/Reportes/"
                      />
                    </FormControl>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card">
            <div className="card-header bg-dark text-white text-center">
              <h1>Descripcion</h1>
            </div>
            <div className="card-body text-center">
              <blockquote className="blockquote">
                <p className="mb-0">
                  <abbr title="Pruebas Jumbo y Sisa" className="initialism">
                    Pruebas de regresión
                  </abbr>
                  a app de JUMBO y SANTA ISABEL (SISA), utilizando tarjeta
                  tokenizadas se realizan pruebas de compra, con metodo de pago
                  debito Y Credito, CatOneClick, JumboOneClick, DebitoOneCLick,
                  SisaCatOneClick, SisaOneClick
                </p>
              </blockquote>
            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </Fragment>
  );
}

/** 

function Inicio(): JSX.Element {
  const [data, setData] = useState([]);

  const history = useHistory();

  const viewHeroeDetails = (id: number) => {
    history.push(`/heroes/${id}`);
  };

  const AlertSuccess = () => {
    return <i className="fa fa-circle text-success pr-1"></i>;
  };

  const AlertDanger = () => {
    return <i className="fa fa-circle text-danger pr-1"></i>;
  };

  const getHeroes = async () => {
    await axios
      .get(baseUrl)
      .then((response) => {
        console.log(response.data);
        setData(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getHeroes();
  }, []);

  return (
    <Fragment>
      <Card>
        <div className="row animated fadeIn">
          <div className="card">
            <div className="card-header bg-dark text-white text-center">
              <h1>Heroes List</h1>
            </div>
            <div className="card-body">
              <table>

              </table>
              <ul className="list-group mb-3">
                {data.map((item: any) => {
                  let volar;
                  if (item.puedeVolar) {
                    volar = <AlertSuccess />;
                  } else {
                    volar = <AlertDanger />;
                  }
                  return (
                    <>
                      <li
                        key={item.id}
                        className="list-group-item d-flex justify-content-between lh-condensed"
                      >
                        <div className="row"></div>
                        <div className="row">
                          <div className="col-md-4 text-center">
                            <img
                              src={item.avatarURL}
                              width="240px"
                              className="img-responsive img-thumbnail my-5"
                            ></img>
                          </div>
                          <div className="col-md-8">
                            <div>
                              <h4 className="d-flex justify-content-between align-items-center pl-4 mb-3">
                                <span className="text-muted">
                                  <strong>{item.nombre}</strong>
                                </span>
                              </h4>
                              <ul className="list-group list-group-flush">
                                <li className="list-group-item">
                                  <h6 className="pl-2 my-0">
                                    <strong>Nombre Real:</strong>
                                    {" " + item.nombreReal}
                                  </h6>
                                </li>
                                <li className="list-group-item">
                                  <h6 className="pl-2 my-0">
                                    puede Volar: {volar}
                                  </h6>
                                </li>
                                <li className="list-group-item">
                                  <h6 className="pl-2 my-0">
                                    Habilidades:
                                    <ul className="list-group mb-3 pl-5">
                                      {item.habilidades.map(
                                        (special: string, i: number) => {
                                          return <li key={i}>{special}</li>;
                                        }
                                      )}
                                    </ul>
                                  </h6>
                                </li>
                                <li className="list-group-item">
                                  <h6 className="pl-2 my-0">
                                    {item.descripcion}
                                  </h6>
                                </li>
                                <li className="list-group-item">
                                  <small className="text-muted">
                                    <button
                                      className="btn btn-dark"
                                      onClick={() => {
                                        viewHeroeDetails(item.id);
                                      }}
                                    >
                                      <em>
                                        Heroe Data.
                                        <i className="fa fa-eye text-white pl-1"></i>
                                      </em>
                                    </button>
                                  </small>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>

                        <div>
                          <span className="badge badge-secondary badge-pill float-right">
                            <i className="fa fa-user pr-1"></i> {item.id}
                          </span>
                        </div>
                      </li>
                    </>
                  );
                })}
              </ul>
            </div>
          </div>
        </div>
      </Card>
    </Fragment>
  );
}*/

//export default Inicio;
