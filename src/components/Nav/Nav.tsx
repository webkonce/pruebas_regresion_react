import React from "react";
import { AppBar, Toolbar, Typography } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink,
} from "react-router-dom";

import Inicio from "../Inicio/Inicio";

import "./Nav.css";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    offset: theme.mixins.toolbar,
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  })
);

function Nav(): JSX.Element {
  const classes = useStyles();

  return (
    <>
      <Router>
        <div className={classes.root}>
          <AppBar position="sticky">
            <Toolbar>
              <Typography variant="h6" className={classes.title}>
                Pruebas Jumbo-SISA-VTEX
              </Typography>
              <div className="btn-group">
                <Link to="/" className="btn btn-dark">
                  Pruebas de Regresión
                </Link>
              </div>
            </Toolbar>
          </AppBar>
          <hr />
          <Switch>
            <Route path="/" exact>
              <Inicio />
            </Route>
          </Switch>
        </div>
      </Router>
    </>
  );
}

/**
 *
 * Datatable and datatable -details 
 *                  <NavLink
                  to="/heroes"
                  className="btn btn-primary"
                  activeClassName="active"
                >
                  Listado de Pruebas
                </NavLink>
 */

export default Nav;
