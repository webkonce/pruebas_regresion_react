export interface Mensaje {
  User: string;
  Prueba: string;
  Test: string;
  App: string[];
  descripcion: string;
  puedeVolar: boolean;
}

/**
 * 
 * {
    "message": {
        "User": "Prueba realizada por: Francisco\n",
        "Prueba": "CATOneClick",
        "Test": "JUMBO_BO_APROBADO",
        "App": "jumboapp",
        "Ruta": "C:/Users/Francisco/Desktop/cencosud/katalon/Reportes/",
        "Reporte": {
            "titulo": "REPORTE",
            "OP": "1115981197277-01",
            "folio": "2021-03-08T14:19:57.0000000+00:00",
            "TID": "57C5E8A0AD3241B28801B5A5CA3CCD51",
            "commercePaymentId": "2492D4C940A2445AA963036B519E3313",
            "MONTO": 8990,
            "authCode": "1213",
            "sequence": "10033268",
            "cliente": "Francisco Roa",
            "medioPago": "CatOneClick",
            "urlCompra": "https://jumboprepro.vtexcommercestable.com.br/checkout/cart/add/?sku=62799&qty=1&seller=1&sc=1",
            "commerceOrderId": "10034044",
            "PCI_VTEX": "https://jumboprepro.myvtex.com/admin/pci-gateway/#/transactions/57C5E8A0AD3241B28801B5A5CA3CCD51",
            "OMS_VTEX": "https://jumboprepro.myvtex.com/admin/checkout/#/orders/seq10034044"
        },
        "Tarjeta": {
            "titulo": "TARJETA",
            "userId": "c1ecff6d-bf5b-407f-b294-e429670ce20c",
            "tarjeta": "Visa",
            "lastDigits": "6623",
            "cardToken": "abfaa313-3602-4a04-bcb0-d9038b1eb296",
            "isFavorite": true,
            "paymentMethod": "CATOneClick"
        }
    }
}
 * 
 */
